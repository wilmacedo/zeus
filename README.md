# Zeus

## Resumo
Plataforma para facilitar a organização dos gastos e cuidados com seu pet. Reunindo de forma fácil e prática os gastos e históricos de suas compras com seu melhor amigo.

## Intuito
Desafio proposto por BlueLab para ser utilizado como treinamento de diversas ferramentas e tecnologias do mercado atual, visando aprimorar os conhecimentos nas áreas desenvolvidas.

## Ideias
- [ ] Registro de gastos
- [ ] Histórico de gastos
- [ ] Múltiplos pets

## Tecnologias Utilizadas

- React Native
- MongoDB
- ReactJS