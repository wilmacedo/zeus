import styled from 'styled-components/native';

export const CardContainer = styled.View`
  margin-top: 40px;
  flex-direction: row;
  justify-content: center;
`;
