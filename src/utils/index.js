export const colorScheme = {
  background: '#fff',
  black: '#000000',
  unselected: '#e8e8e8',
  backgroundLight: '#f2f2f2',
};